using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace U202VisualLauncher
{
    public partial class ChangelogWindow : Form
    {
        bool dragging = false;
        Point draggingGrip;

        public ChangelogWindow()
        {
            InitializeComponent();
        }

        private void butCloseChangelog_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            draggingGrip = e.Location;
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Left += e.X - draggingGrip.X;
                Top += e.Y - draggingGrip.Y;
            }
        }

        private void ChangelogWindow_Load(object sender, EventArgs e)
        {
            txtChangelog.Select(0,0);
        }

    }
}