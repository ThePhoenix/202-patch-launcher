using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

using U202VisualLauncher;


namespace VisualLauncher
{
    public partial class Form1 : Form
    {
        string latestRelease202;
        string currentVersion202 = "unknown";
        string downloadURL202 = @"http://www.gamereplays.org/rotwkunofficial202/portals.php?show=page&name=unofficial-patch-202-download-page";
        //TODO: Update these addresses
        string onlineFileAddress202 = @"http://www.downloads.brothersofwar.org/assets/bfme_downloads/_u202serverinfo.ini";
        string changelogAddress202 = @"http://www.gamereplays.org/rotwkunofficial202/portals.php?show=index&name=rise-of-the-witch-king-unofficial-patch-202-changelog-toc";
        bool useLocalChangelog = false;
        bool installerror202 = false;

        string helpAddress = @"http://www.gamereplays.org/community/index.php?showforum=3709";

        static string installdir = null;
        static string appdatadir = null;
        string videosFolder;
        string[] movieFileNames = { "TE_LOGO", "NLC_LOGO", "CS01", "EALogo" };
        string enabledMovieExtension = ".vp6";
        string disabledMovieExtension = ".disabled";
        bool dragging = false;
        Point draggingGrip;
        string changelogPath = null;
        Dictionary<string, string> localtext = new Dictionary<string, string>();

        class LobbyMusic
        {
            public bool present;
            public bool enabled;
            public bool disabled;
            public string path;
        }
        Dictionary<string, LobbyMusic> lobbyMusicMods;

        public Form1()
        {
            InitializeComponent();
        }

        PrivateFontCollection pfc = new PrivateFontCollection();

        private string Localtext(string key)
        {
            if (localtext.ContainsKey(key))
            {
                return localtext[key];
            }
            else
            {
                return "MISSING:" + key;
            }
        }

        private void LocalizationLoading()
        {
            string localizationFilePath = Path.Combine(installdir, "__launcherlocalization.txt");
            if (!File.Exists(localizationFilePath))
            {
                return;
            }
            using (StreamReader file = new StreamReader(localizationFilePath))
            {

                for (string line = file.ReadLine(); line != null; line = file.ReadLine())
                {
                    if (!line.Contains("="))
                    {
                        continue;
                    }
                    int equalIndex = line.IndexOf("=");
                    string key = line.Substring(0, equalIndex).Trim().ToLowerInvariant();
                    if (key.Length == 0)
                    {
                        continue;
                    }
                    string value = line.Substring(equalIndex + 1).Trim();
                    value = value.Replace(@"\n", "\n");
                    localtext.Add(key, value);
                }
            }

            lblText.Text = Localtext("modstatelabel");
            chkEnable202.Text = Localtext("enable202label");
            lblVersion202.Text = Localtext("noversioninfo");
            lblResolution.Text = Localtext("resolutionlabel");
            chkVersion.Text = Localtext("compatibilitylabel");
            chkVideos.Text = Localtext("videoslabel");
            grpMusic.Text = Localtext("musiclabel");
            radNo.Text = Localtext("musicstandard");
            radGood.Text = Localtext("musicgood");
            radEvil.Text = Localtext("musicevil");
            radBoth.Text = Localtext("musiccombined");
            radSmoke.Text = Localtext("musicsmoke");
            butChangelog.Text = Localtext("changelogbutton");
            butHelp.Text = Localtext("helpbutton");
            butRun.Text = Localtext("startbutton");
            butExit.Text = Localtext("exitbutton");
            this.Text = Localtext("windowtitle");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetDataFromRegistry();

            // Localization Loading. Localized texts in localtext dictionary.
            LocalizationLoading();

            PrepareResolutionList();

            Prepare202VersionList();

            CheckIntrosState();

            RemoveCrashDumpFiles();

            CheckForModFiles();
            
            FixV4();

            GetVersions();

            // Are both enabled? 
            // Impossible: When enabling one, it checks if the other is enabled too and, if so, disables it.
            CheckLobbyMusicMods();

            StartLatestVersionCheckers();
        }

        private void RemoveCrashDumpFiles()
        {
            try
            {
                string[] dumpFiles = Directory.GetFiles(installdir, "DUMP*");

                if (dumpFiles.Length > 0)
                {
                    foreach (string fileName in dumpFiles)
                    {
                        File.Delete(fileName);
                    }
                }
            }
            catch (Exception)
            {
                // This failure is not important enough to warrant any error message at all.
                // Most people don't even know what crash dumps are.
            }
        }

        private void CheckForModFiles()
        {
            // Find U2.02 files
            //string[] basefiles202 = Directory.GetFiles(installdir, "*unofficialpatch202*"); <--- PRE v5.0.0 line
            string[] basefiles202 = Directory.GetFiles(installdir, "*202_*");
            string[] langfiles202 = Directory.GetFiles(installdir, @"lang\*strings202*");
                        
            List<string> files202 = new List<string>();
            files202.AddRange(basefiles202);
            files202.AddRange(langfiles202);

            string [] multiplayerFixFile = Directory.GetFiles(installdir, "!multiplayer.*");
            if (multiplayerFixFile.Length == 1) // if we found the old multiplayer fix, add it to the list
                files202.Add(multiplayerFixFile[0]);

            // Check if a new version is installed over an older, disabled version
            bool deletedOldVersion202 = false;
            
            List<string> filesToDelete = new List<string>();

            try
            {
                foreach (string file in files202)
                {
                    string fileNoExtension = Path.GetFileNameWithoutExtension(file);
                    string extensionless = Path.Combine(Path.GetDirectoryName(file), fileNoExtension);
                    string disabledName = extensionless + ".disabled";
                    string enabledName = extensionless + ".big";
                    bool existsDisabled = File.Exists(disabledName);
                    bool existsEnabled = File.Exists(enabledName);
                    if (existsDisabled && existsEnabled) // if two versions of the same file exist (enabled & disabled), delete disabled one
                    {
                        FileInfo fd = new FileInfo(disabledName);
                        FileInfo fe = new FileInfo(enabledName);

                        deletedOldVersion202 = true;
                        File.Delete(disabledName);
                    }

                    // Clean up old version files

                    string[] oldFiles = { "__unofficialpatch202", "__unofficialpatch202data", "__unofficialpatch202stdmaps",
                                          "__unofficialpatch202wsmaps1", "__unofficialpatch202wsmaps2", "englishstrings202", 
                                          "spanishstrings202", "frenchstrings202", "italianstrings202", "polishstrings202", 
                                          "dutchstrings202", "germanstrings202", "chinese_tstrings202", "!multiplayer", 
                                          "#####unofficialpatch202_v3.9.0" };

                    if (Array.IndexOf(oldFiles, fileNoExtension) > -1)
                    {
                        if (existsDisabled)
                            filesToDelete.Add(disabledName);
                        if (existsEnabled)
                            filesToDelete.Add(enabledName);
                    }
                }
                if (filesToDelete.Count != 0)
                {
                    DialogResult dr = MessageBox.Show(Localtext("u202removeold"), Localtext("warningtitle"), MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        foreach (string file in filesToDelete)
                        {
                            File.Delete(file);
                        }
                        deletedOldVersion202 = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Localtext("u202missingfiles") + ex.Message);
                Close();
            }

            if (deletedOldVersion202)
            {
                MessageBox.Show(Localtext("outdatedu202files"));
                // Redoing file list
                basefiles202 = Directory.GetFiles(installdir, "*202_*");
                langfiles202 = Directory.GetFiles(installdir, @"lang\*strings202*");
                files202.Clear();
                files202.AddRange(basefiles202);
                files202.AddRange(langfiles202);
            }

            int enabled202 = 0, disabled202 = 0;
            foreach (string file in files202)
            {
                FileInfo fi = new FileInfo(file);
               
                if (fi.Extension.ToLower() == ".big")
                {
                    enabled202++;
                }
                else if (fi.Extension.ToLower() != ".big") // previously - (fi.Extension.ToLower() == ".disabled")
                {
                    disabled202++;
                }
            }

            if (disabled202 == 0 && enabled202 != 0)
            {
                lblText.Text = Localtext("u202enabled");
                chkEnable202.Checked = true;
            }
            else if (disabled202 != 0 && enabled202 == 0)
            {
                lblText.Text = Localtext("u202disabled");
                chkEnable202.Checked = false;
            }
            else if (disabled202 == 0 && enabled202 == 0)
            {
                chkEnable202.Visible = false;
                lblStatus202.Visible = false;
                lblVersion202.Visible = false;
                chkEnable202.Enabled = false;
                chkVersion.Visible = false;
                chkVersion.Enabled = false;
                cmbVersion.Visible = false;
                cmbVersion.Enabled = false;
            }
            else if (disabled202 != 0 && enabled202 != 0)
            {
                chkVersion.Checked = true;
            }
        }

        private void StartLatestVersionCheckers()
        {
            if (currentVersion202 != "unknown")
            {
                // Start the latest release checker
                bgw202.RunWorkerAsync();
                lblStatus202.Text = Localtext("lookingforlatestversion");
            }
        }

        private void CheckLobbyMusicMods()
        {
            // Checking lobby music mods

            CheckForMusicMods();
            if (!lobbyMusicMods["evil"].enabled && !lobbyMusicMods["good"].enabled && 
                !lobbyMusicMods["combined"].enabled && !lobbyMusicMods["smoke"].enabled)
            {
                radNo.Checked = true;
            }
            if (!lobbyMusicMods["evil"].present)
            {
                radEvil.Visible = false;
            }
            if (lobbyMusicMods["evil"].enabled)
            {
                radEvil.Checked = true;
            }

            if (!lobbyMusicMods["good"].present)
            {
                radGood.Visible = false;
            }
            if (lobbyMusicMods["good"].enabled)
            {
                radGood.Checked = true;
            }

            if (!lobbyMusicMods["combined"].present)
            {
                radBoth.Visible = false;
            }
            if (lobbyMusicMods["combined"].enabled)
            {
                radBoth.Checked = true;
            }
            if (!lobbyMusicMods["combined"].present)
            {
                radSmoke.Visible = false;
            }
            if (lobbyMusicMods["smoke"].enabled)
            {
                radSmoke.Checked = true;
            }
            if (!lobbyMusicMods["evil"].present && !lobbyMusicMods["good"].present && 
                !lobbyMusicMods["combined"].present && !lobbyMusicMods["smoke"].present)
            {
                grpMusic.Visible = false;
            }
        }

        private void GetVersions()
        {
            // Gets version info 2.02
            try
            {
                string infoFile = Path.Combine(installdir, "_u202info.ini");
                if (File.Exists(infoFile))
                {
                    using (StreamReader sr = new StreamReader(infoFile))
                    {
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            string[] parts = line.Split('=');
                            if (parts[0].Trim().ToLower() == "currentversion")
                            {
                                currentVersion202 = parts[1].Trim();
                                lblVersion202.Text = Localtext("installedversion") + " " + currentVersion202;
                            }
                            else if (parts[0].Trim().ToLower() == "onlinefile")
                            {
                                onlineFileAddress202 = parts[1].Trim().Replace("  ", " ");
                            }
                            else if (parts[0].Trim().ToLower() == "uselocalchangelog")
                            {
                                string s = parts[1].Trim().Replace("  ", " ");
                                if (s.Equals("true"))
                                    useLocalChangelog = true;
                                else if (s.Equals("false"))
                                    useLocalChangelog = false;
                            }
                            else if (parts[0].Trim().ToLower() == "changelogaddress")
                            {
                                changelogAddress202 = parts[1].Trim().Replace("  ", " ");
                            }
                        }
                    }
                }
            }
            catch
            {
                // If the online file is not found, the program will know to print an error
            }

            // Gets previously-selected Compatibility Mode version from a neutral file
            try
            {
                string infoFile = Path.Combine(installdir, "_launchercontrol.ini");
                if (File.Exists(infoFile))
                {
                    chkVersion.Checked = false;
                    using (StreamReader sr = new StreamReader(infoFile))
                    {
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            string[] parts = line.Split('=');
                            if (parts[0].Trim().ToLower() == "u202version")
                            {
                                chkVersion.Checked = true;
                                cmbVersion.SelectedItem = parts[1].Trim().Replace("  ", " ");
                            }
                        }
                    }
                }
            }
            catch
            {
                // While unfortunate, failure to read this data can safely be hidden.
            }
        }

        private void CheckIntrosState()
        {
            // Checking state of intro movies
            int enabledMovies = 0, disabledMovies = 0;
            foreach (string filename in movieFileNames)
            {
                string enabledMoviePath = Path.Combine(videosFolder, filename + enabledMovieExtension);
                string disabledMoviePath = Path.Combine(videosFolder, filename + disabledMovieExtension);
                if (File.Exists(enabledMoviePath))
                {
                    enabledMovies++;
                }
                if (File.Exists(disabledMoviePath))
                {
                    disabledMovies++;
                }
            }
            if (enabledMovies != 0 && disabledMovies == 0)
            {
                chkVideos.CheckState = CheckState.Checked;
            }
            else if (enabledMovies == 0 && disabledMovies != 0)
            {
                chkVideos.CheckState = CheckState.Unchecked;
            }
            else
            {
                chkVideos.CheckState = CheckState.Indeterminate;
            }
        }

        private void PrepareResolutionList()
        {
            string[] resolutions = { "640 480", "640 512", "720 348", "720 350", "720 360", "800 480", "800 600", 
                                     "832 624", "1024 768", "1120 832", "1152 864", "1152 900", "1280 768", "1280 800", 
                                     "1280 854", "1280 960", "1280 1024", "1366 768", "1400 1050", "1440 900", "1600 900",
                                     "1600 1024", "1600 1200", "1680 1050", "1920 1080", "1920 1200", "1920 1400", "2048 1536", 
                                     "2560 1600", "2560 2048", "2800 2100", "3200 2048", "3200 2400", "3840 2400", "4096 2160"};

            cmbResolution.Items.Clear();
            cmbResolution.Items.AddRange(resolutions);

            Rectangle screenArea = Screen.PrimaryScreen.Bounds; // Checking current desktop resolution
            string monitorResolution = screenArea.Width + " " + screenArea.Height;
            string previousResolution = null;

            // Gets last selected resolution from Options.ini
            try
            {
                string optionsFile = Path.Combine(appdatadir, "Options.ini");
                if (File.Exists(optionsFile))
                {
                    using (StreamReader sr = new StreamReader(optionsFile))
                    {
                        while (!sr.EndOfStream)
                        {
                            string[] parts = sr.ReadLine().Split('=');
                            /*if (parts[0].Trim() == "Resolution")
                            {
                                previousResolution = parts[1].Trim().Replace("  ", " x ");
                            }*/
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            if (previousResolution == null)
            {
                previousResolution = monitorResolution;
            }

            bool addMonitorResolution = true;
            bool addPreviousResolution = true;

            foreach (string res in resolutions)
            {
                if (res == monitorResolution)
                {
                    addMonitorResolution = false;
                }

                if (res == previousResolution)
                {
                    addPreviousResolution = false;
                }
            }

            if (addMonitorResolution)
            {
                cmbResolution.Items.Add(monitorResolution);
            }

            if (addPreviousResolution)
            {
                cmbResolution.Items.Add(previousResolution);
            }

            cmbResolution.SelectedItem = previousResolution;
            cmbResolution.Enabled = true;
        }

        private void Prepare202VersionList()
        {
            List<string> versions = new List<string>();
            string currentversion = "";

            try
            {
                string infoFile = Path.Combine(installdir, "_u202info.ini");
                if (File.Exists(infoFile))
                {
                    using (StreamReader sr = new StreamReader(infoFile))
                    {
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            string[] parts = line.Split('=');
                            if (parts[0].Trim() == "gameversion")
                            {
                                versions.Add(parts[1].Trim());
                            }
                            else if (parts[0].Trim() == "currentversion")
                            {
                                currentversion = parts[1].Trim();
                            }
                        }
                    }
                }

                cmbVersion.Items.Clear();
                cmbVersion.Items.AddRange(versions.ToArray());
                cmbVersion.SelectedItem = versions[0];

                //string[] basefiles = Directory.GetFiles(installdir, "*unofficialpatch202_v*"); PRE v5.0
                string[] basefiles = Directory.GetFiles(installdir, "*202_v*"); 

                for (int i = 0; i < versions.Count; i++)
                {
                    //versions[i] = "unofficialpatch202_v" + versions[i]; PRE v5.0
                    versions[i] = "202_v" + versions[i];
                    bool found = false;

                    foreach (string filename in basefiles)
                    {
                        if (filename.Contains(versions[i]))
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        DialogResult dr = MessageBox.Show(Localtext("errormustreinstall"), Localtext("installerror"), MessageBoxButtons.YesNo);
                        if (dr == DialogResult.Yes)
                        {
                            System.Diagnostics.Process.Start(downloadURL202);
                            Close();
                        }
                        installerror202 = true;
                        break;
                    }
                }

                // currentversion = "unofficialpatch202_v" + currentversion; PRE v5.0
                bool foundCurrent = false;
                foreach (string versionFound in basefiles)
                {
                    if (versionFound.Contains(currentversion))
                    {
                        foundCurrent = true;
                        break;
                    }
                }

                if (!foundCurrent)
                {
                    DialogResult dr = MessageBox.Show(Localtext("errormustreinstall"), Localtext("installerror"), MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start(downloadURL202);
                        Close();
                    }
                    installerror202 = true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                Close();
            }
        }

        private void GetDataFromRegistry()
        {
            // Get data from Registry
            installdir = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king", "InstallPath", null) as string;
            appdatadir = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king", "UserDataLeafName", null) as string;
            if (installdir == null)
            {
                // Are we on a x64 Windows?
                installdir = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king", "InstallPath", null) as string;
                appdatadir = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king", "UserDataLeafName", null) as string;
                if (installdir == null || appdatadir == null)
                {
                    MessageBox.Show(Localtext("errorreinstallgame"));
                    Close();
                }
            }
            appdatadir = Path.Combine(Environment.GetEnvironmentVariable("APPDATA"), appdatadir);
            videosFolder = Path.Combine(installdir, @"data\movies");

            FixOptionsIni(appdatadir, installdir);
        }

        private void CheckForMusicMods()
        {
            // Lobby Music mods files
            List<string> filesLobbyMusic = new List<string>();
            filesLobbyMusic.AddRange(Directory.GetFiles(installdir, "*lobbymusic*"));


            lobbyMusicMods = new Dictionary<string, LobbyMusic>();
            lobbyMusicMods.Add("good", new LobbyMusic());
            lobbyMusicMods.Add("evil", new LobbyMusic());
            lobbyMusicMods.Add("combined", new LobbyMusic());
            lobbyMusicMods.Add("smoke", new LobbyMusic());

            foreach (string file in filesLobbyMusic)
            {
                if (MusicModIsOld(file))    // Cleanup older files
                {
                    File.Delete(file);
                }
                else if (file.Contains("___lobbymusic-EvilThemed-micromod")) // previously "_lobbymusic-micromod"
                {
                    lobbyMusicMods["evil"].present = true;

                    if (file.EndsWith(".big"))
                    {
                        lobbyMusicMods["evil"].enabled = true;
                    }
                    else if (file.EndsWith(".disabled"))
                    {
                        if (lobbyMusicMods["evil"].enabled)
                        {
                            File.Delete(file);
                            continue;
                        }
                        lobbyMusicMods["evil"].disabled = true;
                    }
                    lobbyMusicMods["evil"].path = file;
                }
                else if (file.Contains("___lobbymusic-GoodThemed-micromod")) // previously __lobbymusic-GoodThemed-micromod
                {
                    lobbyMusicMods["good"].present = true;
                    lobbyMusicMods["good"].path = file;

                    if (file.EndsWith(".big"))
                    {
                        lobbyMusicMods["good"].enabled = true;
                    }
                    else if (file.EndsWith(".disabled"))
                    {
                        if (lobbyMusicMods["good"].enabled)
                        {
                            File.Delete(file);
                            continue;
                        }

                        lobbyMusicMods["good"].disabled = true;
                    }
                    lobbyMusicMods["good"].path = file;
                }
                else if (file.Contains("___lobbymusic-TheSmokingManTracks-micromod"))
                {
                    lobbyMusicMods["smoke"].present = true;
                    lobbyMusicMods["smoke"].path = file;

                    if (file.EndsWith(".big"))
                    {
                        lobbyMusicMods["smoke"].enabled = true;
                    }
                    else if (file.EndsWith(".disabled"))
                    {
                        if (lobbyMusicMods["smoke"].enabled)
                        {
                            File.Delete(file);
                            continue;
                        }

                        lobbyMusicMods["smoke"].disabled = true;
                    }
                    lobbyMusicMods["smoke"].path = file;
                }
                if (file.Contains("___lobbymusic-Combined-micromod")) // __lobbymusic-Combined-micromod
                {
                    lobbyMusicMods["combined"].present = true;
                    lobbyMusicMods["combined"].path = file;

                    if (file.EndsWith(".big"))
                    {
                        lobbyMusicMods["combined"].enabled = true;
                    }
                    else if (file.EndsWith(".disabled"))
                    {
                        if (lobbyMusicMods["combined"].enabled)
                        {
                            File.Delete(file);
                            continue;
                        }

                        lobbyMusicMods["combined"].disabled = true;
                    }
                    lobbyMusicMods["combined"].path = file;
                }
            }
        }

        // Decide if music mod should be deleted
        private bool MusicModIsOld(string name)
        {
            return name.Contains("_lobbymusic-micromod") ||
                (name.Contains("__lobbymusic-EvilThemed-micromod") && !name.Contains("___lobbymusic-EvilThemed-micromod")) ||
                (name.Contains("__lobbymusic-GoodThemed-micromod") && !name.Contains("___lobbymusic-GoodThemed-micromod")) ||
                (name.Contains("__lobbymusic-Combined-micromod") && !name.Contains("___lobbymusic-Combined-micromod"));
        }

        private void chkEnable202_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkEnable202.Checked == true)
                {
                    bool isv4 = false;
                    if ((chkVersion.Checked == true) && (chkVersion.Text == "4.0.0"))
                        isv4 = true; 
                    PerformEnable202(isv4);

                    lblText.Text = Localtext("u202enabled");

                    // Look for the u202 Changelog.
                    string path = Path.Combine(installdir, "_U202Changelog.txt");
                    if (File.Exists(path))
                    {
                        changelogPath = path;
                        butChangelog.Visible = true;
                    }
                    else
                    {
                        changelogPath = null;
                        butChangelog.Visible = false;
                    }

                }
                else if (chkEnable202.Checked == false)
                {
                    PerformDisable202();
                    lblText.Text = Localtext("u202disabled");
                    if (chkVersion.Checked == true)
                    {
                        chkVersion.Checked = false;
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show(Localtext("errortouchingu202files"));
                Close();
            }
        }

        private static void PerformDisable202()
        {
            //string[] basefiles = Directory.GetFiles(installdir, "*unofficialpatch202*"); PRE v5.0
            string[] basefiles = Directory.GetFiles(installdir, "*202_v*");
            string[] langfiles = Directory.GetFiles(installdir, @"lang\*strings202*");
            List<string> files = new List<string>();

            files.AddRange(basefiles);
            files.AddRange(langfiles);


            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                /*
                if (fi.FullName.Contains("englishstrings202_v3.6.2")) // Fix the improperly named v4.0.0 string file
                    File.Move(file, fi.FullName.Replace("englishstrings202_v3.6.2", "englishstrings202_v4.0.0"));*/

                File.Move(file, fi.FullName.Replace(".big", ".disabled"));
            }
        }

        private static void PerformEnable202(bool isOnV4)
        {
            //string[] basefiles = Directory.GetFiles(installdir, "*unofficialpatch202_v*"); // previously "*unofficialpatch202*");
            string[] basefiles = Directory.GetFiles(installdir, "*202_v*");
            string[] langfiles = Directory.GetFiles(installdir, @"lang\*strings202_v*"); // previously @"lang\*strings202*");
            List<string> files = new List<string>();

            files.AddRange(basefiles);
            files.AddRange(langfiles);

            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                /*
                if (fi.FullName.Contains("englishstrings202_v3.6.2")) // Fix the improperly named v4.0.0 string file
                    File.Move(file, fi.FullName.Replace("englishstrings202_v3.6.2", "englishstrings202_v4.0.0"));*/
                if (!fi.FullName.Contains("202_v4.0.0") || isOnV4)
                    File.Move(file, fi.FullName.Replace(".disabled", ".big"));
            }
        }

        //This function is apparently not used, so let's comment it out...
        /*
        private static void PerformAddNewerFiles202(string v)
        {
            string[] vf = Directory.GetFiles(installdir, "*202_v" + v + "*");
            string[] basefiles = Directory.GetFiles(installdir, "*202_v*"); //@"*unofficialpatch202*");
            string[] langfiles = Directory.GetFiles(installdir, @"lang\*strings202_v*"); //@"lang\*strings202*");
            List<string> files = new List<string>();

            if (vf.Length >= 1)
            {
                string[] ver = vf[0].Split('v');

                foreach (string file in basefiles) //core files
                {
                    string[] f = file.Split('v');
                    
                    if (f[1].CompareTo(ver[1]) <= 0)
                    {
                        files.Add(file);
                    }
                }

                foreach (string file in langfiles) //strings
                {
                    string[] f = file.Split('v');
                    if (f[1].CompareTo(ver[1]) <= 0)
                    {
                        files.Add(file);
                    }
                }

                foreach (string file in files)
                {
                    FileInfo fi = new FileInfo(file);

                    File.Move(file, fi.FullName.Replace(".disabled", ".big"));
                }
            }
        }*/

        private static void PerformRemoveNewerFiles202(string v)
        {
            //string[] vf = Directory.GetFiles(installdir, "*unofficialpatch202_v" + v + "*"); PRE v5.0
            string[] vf = Directory.GetFiles(installdir, "*202_v" + v + "*");
            //string[] basefiles = Directory.GetFiles(installdir, "*unofficialpatch202_v*"); PRE v5.0 //@"*unofficialpatch202*");
            string[] basefiles = Directory.GetFiles(installdir, "*202_v*");
            string[] langfiles = Directory.GetFiles(installdir, @"lang\*strings202_v*"); //@"lang\*strings202*");
            List<string> files = new List<string>();

            if (vf.Length >= 1)
            {
                string[] ver = vf[0].Split('v');

                foreach (string file in basefiles)
                {
                    string[] f = file.Split('v');
                    if ((f[1].CompareTo(ver[1]) > 0) || ((f[1].CompareTo(ver[1]) < 0) && f[1] == "4.0.0.big")) //4.0 is always disabled for new patches
                    {
                        files.Add(file);
                    }
                    else if ((f[1].CompareTo(ver[1]) == 0) && f[1] == "4.0.0.big")
                    {
                        FileInfo fx = new FileInfo(file);
                        File.Move(file, fx.FullName.Replace(".disabled", ".big"));
                    }
                }

                foreach (string file in langfiles)
                {
                    string[] f = file.Split('v');
                    if (f[1].CompareTo(ver[1]) > 0)
                    {
                        files.Add(file);
                    }
                }

                foreach (string file in files)
                {
                    FileInfo fi = new FileInfo(file);
                    File.Move(file, fi.FullName.Replace(".big", ".disabled"));
                }
            }
        }

        private void butRun_Click(object sender, EventArgs e)
        {
            // TODO v5.1: Find out why this throws No-cd error when game is launched with WinCDEmu
            try
            {
                System.Diagnostics.Process.Start(Path.Combine(installdir, @"lotrbfme2ep1.exe"));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Close();
            }
        }

        private void bgw202_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!installerror202)
            {
                try
                {
                    string tempFile = Path.Combine(installdir, "_u202serverinfo.ini");
                    DownloadData dd = DownloadData.Create(onlineFileAddress202, tempFile);

                    Stream s = dd.DownloadStream;
                    using (StreamReader sr = new StreamReader(s))
                    {
                        while (!sr.EndOfStream)
                        {
                            string line = sr.ReadLine();
                            string[] parts = line.Split('=');
                            if (parts[0].Trim().ToLower() == "lastestrelease" || parts[0].Trim().ToLower() == "latestrelease")
                            {
                                latestRelease202 = parts[1].Trim();
                            }
                            else if (parts[0].Trim().ToLower() == "downloadurl")
                            {
                                downloadURL202 = parts[1].Trim();
                            }
                        }
                    }
                    dd.Close();
                    if (File.Exists(tempFile))
                    {
                        File.Delete(tempFile);
                    }
                }
                catch
                {
                    // If the online file is not found, the program will know to print an error
                }
            }
        }

        private void bgw202_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (installerror202)
            {
                lblVersion202.Text = Localtext("installerror");
                lblStatus202.Text = Localtext("installerror");
            }
            else if (latestRelease202 != null)
            {
                lblVersion202.Text = Localtext("installedversion") + " " + currentVersion202 + "\n" + Localtext("latestversion") + " " + latestRelease202;
                if (latestRelease202.CompareTo(currentVersion202) > 0)
                {
                    lblStatus202.Text = Localtext("u202outdated");
                    lblStatus202.ForeColor = Color.Red;
                    string message;
                    if (downloadURL202 == null)
                    {
                        message = Localtext("u202new") + latestRelease202;
                    }
                    else
                    {
                        message = Localtext("u202new") + " " + latestRelease202 + "\n" + Localtext("downloadpageask");
                    }
                    DialogResult dr = MessageBox.Show(message, Localtext("newversiontitle"), MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        System.Diagnostics.Process.Start(downloadURL202);
                        Close();
                    }
                }
                else if (installerror202)
                {
                    lblVersion202.Text = Localtext("installerror");
                    lblStatus202.Text = Localtext("installerror");
                }
                else if (latestRelease202.CompareTo(currentVersion202) < 0)
                {
                    lblStatus202.Text = Localtext("usingunreleased");
                }
                else
                {
                    lblStatus202.Text = Localtext("u202uptodate");
                }
            }
            else
            {
                lblStatus202.Text = Localtext("cannotfindversionserver");
            }
        }

        private void chkVersion_CheckedChanged(object sender, EventArgs e)
        {
            FixV4();
            cmbVersion.Enabled = chkVersion.Checked;

            try
            {
                if (chkVersion.Checked == true)
                {
                    if (chkEnable202.Checked == false)
                    {
                        chkEnable202.Checked = true;
                    }
                    PerformRemoveNewerFiles202(cmbVersion.SelectedItem.ToString());
                }
                else if (chkVersion.Checked == false)
                {
                    if (chkEnable202.Checked == true)
                    {
                        PerformEnable202(false);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show(Localtext("errortouchingu202files"));
                Close();
            }
        }

        private void cmbVersion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chkVersion.Checked == true)
            {
                try
                {
                    bool isV4 = false;
                    if (cmbVersion.Text == "4.0.0")
                        isV4 = true;

                    PerformEnable202(isV4);
                    PerformRemoveNewerFiles202(cmbVersion.SelectedItem.ToString());
                }
                catch (Exception)
                {
                    MessageBox.Show(Localtext("errortouchingu202files"));
                    Close();
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                string infoFile = Path.Combine(installdir, "_launchercontrol.ini");
                using (StreamWriter sw = new StreamWriter(infoFile, false))
                {
                    if (chkVersion.Checked)
                    {
                        sw.WriteLine("u202version={0}", cmbVersion.SelectedItem.ToString());
                    }
                }
            }
            catch
            {
                // While unfortunate, failure to write this data is nonessential and does not warrant an error message.
            }

            try
            {
                string[] lines = File.ReadAllLines(Path.Combine(appdatadir, "Options.ini"));
                bool resolutionExists = false; 

                for (int i = 0; i < lines.Length; i++)
                {
                    if (String.Compare(lines[i], 0, "Resolution", 0, 10) == 0)
                    {
                        lines[i] = "Resolution = " + cmbResolution.SelectedItem.ToString();
                        resolutionExists = true;
                    }
                }
                if (resolutionExists)
                {
                    File.WriteAllLines(Path.Combine(appdatadir, "Options.ini"), lines);
                }
                else
                {
                    StreamWriter sw = new StreamWriter(Path.Combine(appdatadir, "Options.ini"), true);
                    sw.Write("\nResolution = " + cmbResolution.SelectedItem.ToString().Replace(" x ", " "));
                    sw.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void chkVideos_CheckedChanged(object sender, EventArgs e)
        {
            foreach (string filename in movieFileNames)
            {
                string oldPath, newPath;
                if (chkVideos.Checked)
                {
                    oldPath = Path.Combine(videosFolder, filename + disabledMovieExtension);
                    newPath = Path.Combine(videosFolder, filename + enabledMovieExtension);
                }
                else
                {
                    oldPath = Path.Combine(videosFolder, filename + enabledMovieExtension);
                    newPath = Path.Combine(videosFolder, filename + disabledMovieExtension);
                }
                try
                {
                    File.Move(oldPath, newPath);
                }
                catch (Exception)
                {
                    // This exception will be thrown the first time the launcher is opened on a new install.
                    // Or when movies are indeterminate.  The launcher will fix this, so do not warn.
                    // If an OS error prevents file writing, too bad, the other cases are way more likely.
                }
                if (chkVideos.CheckState == CheckState.Indeterminate)
                {
                    File.Delete(oldPath);
                }
            }
        }

        private void butExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            draggingGrip = e.Location;
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Left += e.X - draggingGrip.X;
                Top += e.Y - draggingGrip.Y;
            }
        }

        private void radNo_CheckedChanged(object sender, EventArgs e)
        {
            if (radNo.Checked == true)
            {
                PerformDisableLobbyMusic("evil");
                PerformDisableLobbyMusic("combined");
                PerformDisableLobbyMusic("good");
                PerformDisableLobbyMusic("smoke");
            }

        }

        private void radGood_CheckedChanged(object sender, EventArgs e)
        {
            CheckForMusicMods();

            if (radGood.Checked == true)
            {
                PerformDisableLobbyMusic("evil");
                PerformDisableLobbyMusic("combined");
                PerformDisableLobbyMusic("smoke");
                PerformEnableLobbyMusic("good");
            }
        }

        private void PerformDisableLobbyMusic(string what)
        {
            if (lobbyMusicMods[what].enabled)
            {
                string filename = lobbyMusicMods[what].path;
                lobbyMusicMods[what].path = filename.Replace(".big", ".disabled");
                File.Move(filename, lobbyMusicMods[what].path);
            }
        }

        private void PerformEnableLobbyMusic(string what)
        {
            if (lobbyMusicMods[what].disabled)
            {
                string filename = lobbyMusicMods[what].path;
                lobbyMusicMods[what].path = filename.Replace(".disabled", ".big");
                File.Move(filename, lobbyMusicMods[what].path);
            }
        }

        private void radEvil_CheckedChanged(object sender, EventArgs e)
        {
            CheckForMusicMods();

            if (radEvil.Checked == true)
            {
                PerformDisableLobbyMusic("good");
                PerformDisableLobbyMusic("combined");
                PerformDisableLobbyMusic("smoke");
                PerformEnableLobbyMusic("evil");
            }

        }

        private void radBoth_CheckedChanged(object sender, EventArgs e)
        {
            CheckForMusicMods();

            if (radBoth.Checked == true)
            {
                PerformDisableLobbyMusic("evil");
                PerformDisableLobbyMusic("good");
                PerformDisableLobbyMusic("smoke");
                PerformEnableLobbyMusic("combined");
            }

        }

        private void radSmoke_CheckedChanged(object sender, EventArgs e)
        {
            CheckForMusicMods();

            if (radSmoke.Checked == true)
            {
                PerformDisableLobbyMusic("evil");
                PerformDisableLobbyMusic("good");
                PerformDisableLobbyMusic("combined");
                PerformEnableLobbyMusic("smoke");
            }

        }

        //Create Options.ini if it doesn't already exist
        private void FixOptionsIni(string appdata, string installdir)
        {
            if (!File.Exists(appdatadir + "\\Options.ini"))
            {
                File.Create(installdir + "\\Options.ini").Dispose();

                using (TextWriter tw = new StreamWriter(appdatadir + "\\Options.ini"))
                {
                    tw.WriteLine("AllHealthBars = yes\nAudioLOD = High\nIdealStaticGameLOD = VeryLow");
                    tw.Close();
                }
            }
        }

        private void butChangelog_Click(object sender, EventArgs e)
        {
            if (changelogPath == null)
            {
                return;
            }
            if (useLocalChangelog || !changelogPath.Contains("202"))
            {
                try
                {
                    ChangelogWindow win = new ChangelogWindow();
                    string text;
                    using (StreamReader changelogfile = new StreamReader(changelogPath))
                    {
                        text = changelogfile.ReadToEnd();
                        win.txtChangelog.Text = text;
                    }
                    win.Top = Top;
                    win.Left = Left;
                    win.Text = Localtext("changelogtitle");
                    win.butCloseChangelog.Text = Localtext("changelogclosebutton");
                    win.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                System.Diagnostics.Process.Start(changelogAddress202);
            }
        }

        private void butHelp_Click( object sender, EventArgs e )
        {
            System.Diagnostics.Process.Start( helpAddress );
        }

        private void FixV4()
        {
            string [] basefiles202 = Directory.GetFiles(installdir, "*202_v4.0.0*");

            foreach (string file in basefiles202)
            {
                FileInfo fi = new FileInfo(file);

                if (fi.FullName.Contains("202_v4.0.0"))
                {
                    if ((chkVersion.Checked) && (cmbVersion.Text == "4.0.0"))
                    {
                        File.Move(file, fi.FullName.Replace(".disabled", ".big"));
                    }
                    else
                    {
                        File.Move(file, fi.FullName.Replace(".big", ".disabled"));
                    }
                }
            }
        }


    }
}