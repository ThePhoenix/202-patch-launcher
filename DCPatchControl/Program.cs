﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace VisualLauncher
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // First of all, we want just one instance of this application
            string processName = Process.GetCurrentProcess().ProcessName;
            Process[] processCollection = Process.GetProcessesByName(processName);
            if (processCollection.Length > 1)
            {
                // If there is another, we shut down.
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}