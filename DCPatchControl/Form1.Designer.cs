﻿namespace VisualLauncher
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.lblText = new System.Windows.Forms.Label();
            this.butRun = new System.Windows.Forms.Button();
            this.cmbResolution = new System.Windows.Forms.ComboBox();
            this.lblResolution = new System.Windows.Forms.Label();
            this.cmbVersion = new System.Windows.Forms.ComboBox();
            this.chkVersion = new System.Windows.Forms.CheckBox();
            this.chkVideos = new System.Windows.Forms.CheckBox();
            this.butExit = new System.Windows.Forms.Button();
            this.chkEnable202 = new System.Windows.Forms.CheckBox();
            this.grpMusic = new System.Windows.Forms.GroupBox();
            this.radSmoke = new System.Windows.Forms.RadioButton();
            this.radBoth = new System.Windows.Forms.RadioButton();
            this.radEvil = new System.Windows.Forms.RadioButton();
            this.radGood = new System.Windows.Forms.RadioButton();
            this.radNo = new System.Windows.Forms.RadioButton();
            this.lblStatus202 = new System.Windows.Forms.Label();
            this.lblVersion202 = new System.Windows.Forms.Label();
            this.bgw202 = new System.ComponentModel.BackgroundWorker();
            this.butHelp = new System.Windows.Forms.Button();
            this.butChangelog = new System.Windows.Forms.Button();
            this.grpMusic.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblText
            // 
            this.lblText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblText.BackColor = System.Drawing.Color.Transparent;
            this.lblText.Font = new System.Drawing.Font("Bookman Old Style", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblText.ForeColor = System.Drawing.Color.White;
            this.lblText.Location = new System.Drawing.Point(43, 43);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(434, 117);
            this.lblText.TabIndex = 0;
            this.lblText.Text = "2.02 is enabled. ";
            this.lblText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblText.UseCompatibleTextRendering = true;
            this.lblText.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.lblText.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.lblText.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // butRun
            // 
            this.butRun.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butRun.BackColor = System.Drawing.Color.Transparent;
            this.butRun.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("butRun.BackgroundImage")));
            this.butRun.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butRun.FlatAppearance.BorderSize = 0;
            this.butRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butRun.Font = new System.Drawing.Font("Bookman Old Style", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butRun.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.butRun.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butRun.Location = new System.Drawing.Point(50, 507);
            this.butRun.Name = "butRun";
            this.butRun.Size = new System.Drawing.Size(208, 40);
            this.butRun.TabIndex = 12;
            this.butRun.Text = "Start Game";
            this.butRun.UseVisualStyleBackColor = false;
            this.butRun.Click += new System.EventHandler(this.butRun_Click);
            this.butRun.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.butRun.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.butRun.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // cmbResolution
            // 
            this.cmbResolution.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbResolution.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbResolution.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbResolution.BackColor = System.Drawing.Color.White;
            this.cmbResolution.DropDownHeight = 100;
            this.cmbResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbResolution.Enabled = false;
            this.cmbResolution.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbResolution.Font = new System.Drawing.Font("Book Antiqua", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbResolution.ForeColor = System.Drawing.Color.Black;
            this.cmbResolution.FormatString = "##### #####";
            this.cmbResolution.IntegralHeight = false;
            this.cmbResolution.Location = new System.Drawing.Point(293, 443);
            this.cmbResolution.Name = "cmbResolution";
            this.cmbResolution.Size = new System.Drawing.Size(132, 25);
            this.cmbResolution.TabIndex = 11;
            this.cmbResolution.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.cmbResolution.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.cmbResolution.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // lblResolution
            // 
            this.lblResolution.BackColor = System.Drawing.Color.Transparent;
            this.lblResolution.Font = new System.Drawing.Font("Book Antiqua", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResolution.ForeColor = System.Drawing.Color.White;
            this.lblResolution.Location = new System.Drawing.Point(287, 413);
            this.lblResolution.Name = "lblResolution";
            this.lblResolution.Size = new System.Drawing.Size(182, 27);
            this.lblResolution.TabIndex = 6;
            this.lblResolution.Text = "Screen Resolution:";
            this.lblResolution.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.lblResolution.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.lblResolution.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // cmbVersion
            // 
            this.cmbVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbVersion.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbVersion.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbVersion.BackColor = System.Drawing.Color.White;
            this.cmbVersion.DropDownHeight = 100;
            this.cmbVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVersion.Enabled = false;
            this.cmbVersion.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cmbVersion.Font = new System.Drawing.Font("Book Antiqua", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVersion.ForeColor = System.Drawing.Color.Black;
            this.cmbVersion.FormatString = "##### #####";
            this.cmbVersion.IntegralHeight = false;
            this.cmbVersion.Location = new System.Drawing.Point(107, 397);
            this.cmbVersion.Name = "cmbVersion";
            this.cmbVersion.Size = new System.Drawing.Size(90, 25);
            this.cmbVersion.TabIndex = 9;
            this.cmbVersion.SelectedIndexChanged += new System.EventHandler(this.cmbVersion_SelectedIndexChanged);
            this.cmbVersion.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.cmbVersion.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.cmbVersion.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // chkVersion
            // 
            this.chkVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkVersion.BackColor = System.Drawing.Color.Transparent;
            this.chkVersion.Font = new System.Drawing.Font("Book Antiqua", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkVersion.ForeColor = System.Drawing.Color.White;
            this.chkVersion.Location = new System.Drawing.Point(50, 358);
            this.chkVersion.Name = "chkVersion";
            this.chkVersion.Size = new System.Drawing.Size(203, 33);
            this.chkVersion.TabIndex = 8;
            this.chkVersion.Text = "Use older version: ";
            this.chkVersion.UseVisualStyleBackColor = false;
            this.chkVersion.CheckedChanged += new System.EventHandler(this.chkVersion_CheckedChanged);
            this.chkVersion.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.chkVersion.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.chkVersion.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // chkVideos
            // 
            this.chkVideos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkVideos.BackColor = System.Drawing.Color.Transparent;
            this.chkVideos.Font = new System.Drawing.Font("Book Antiqua", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkVideos.ForeColor = System.Drawing.Color.White;
            this.chkVideos.Location = new System.Drawing.Point(50, 449);
            this.chkVideos.Name = "chkVideos";
            this.chkVideos.Size = new System.Drawing.Size(203, 24);
            this.chkVideos.TabIndex = 10;
            this.chkVideos.Text = "Show intro movies";
            this.chkVideos.UseVisualStyleBackColor = false;
            this.chkVideos.CheckedChanged += new System.EventHandler(this.chkVideos_CheckedChanged);
            this.chkVideos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.chkVideos.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.chkVideos.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // butExit
            // 
            this.butExit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butExit.BackColor = System.Drawing.Color.Transparent;
            this.butExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("butExit.BackgroundImage")));
            this.butExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.butExit.FlatAppearance.BorderSize = 0;
            this.butExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butExit.Font = new System.Drawing.Font("Bookman Old Style", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butExit.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.butExit.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butExit.Location = new System.Drawing.Point(269, 507);
            this.butExit.Name = "butExit";
            this.butExit.Size = new System.Drawing.Size(208, 40);
            this.butExit.TabIndex = 13;
            this.butExit.Text = "Exit";
            this.butExit.UseVisualStyleBackColor = false;
            this.butExit.Click += new System.EventHandler(this.butExit_Click);
            this.butExit.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.butExit.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.butExit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // chkEnable202
            // 
            this.chkEnable202.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkEnable202.BackColor = System.Drawing.Color.Transparent;
            this.chkEnable202.Font = new System.Drawing.Font("Book Antiqua", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEnable202.ForeColor = System.Drawing.Color.Transparent;
            this.chkEnable202.Location = new System.Drawing.Point(79, 154);
            this.chkEnable202.Name = "chkEnable202";
            this.chkEnable202.Size = new System.Drawing.Size(167, 26);
            this.chkEnable202.TabIndex = 1;
            this.chkEnable202.Text = "Enable 2.02";
            this.chkEnable202.UseVisualStyleBackColor = false;
            this.chkEnable202.CheckedChanged += new System.EventHandler(this.chkEnable202_CheckedChanged);
            this.chkEnable202.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.chkEnable202.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.chkEnable202.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // grpMusic
            // 
            this.grpMusic.BackColor = System.Drawing.Color.Transparent;
            this.grpMusic.Controls.Add(this.radSmoke);
            this.grpMusic.Controls.Add(this.radBoth);
            this.grpMusic.Controls.Add(this.radEvil);
            this.grpMusic.Controls.Add(this.radGood);
            this.grpMusic.Controls.Add(this.radNo);
            this.grpMusic.Font = new System.Drawing.Font("Book Antiqua", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpMusic.ForeColor = System.Drawing.Color.White;
            this.grpMusic.Location = new System.Drawing.Point(269, 254);
            this.grpMusic.Name = "grpMusic";
            this.grpMusic.Size = new System.Drawing.Size(208, 156);
            this.grpMusic.TabIndex = 10;
            this.grpMusic.TabStop = false;
            this.grpMusic.Text = "Lobby Music";
            // 
            // radSmoke
            // 
            this.radSmoke.AutoSize = true;
            this.radSmoke.Font = new System.Drawing.Font("Book Antiqua", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radSmoke.ForeColor = System.Drawing.Color.White;
            this.radSmoke.Location = new System.Drawing.Point(6, 123);
            this.radSmoke.Name = "radSmoke";
            this.radSmoke.Size = new System.Drawing.Size(171, 22);
            this.radSmoke.TabIndex = 8;
            this.radSmoke.Text = "Smoking Man\'s Tracks";
            this.radSmoke.UseVisualStyleBackColor = true;
            this.radSmoke.CheckedChanged += new System.EventHandler(this.radSmoke_CheckedChanged);
            // 
            // radBoth
            // 
            this.radBoth.AutoSize = true;
            this.radBoth.Font = new System.Drawing.Font("Book Antiqua", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radBoth.ForeColor = System.Drawing.Color.White;
            this.radBoth.Location = new System.Drawing.Point(6, 95);
            this.radBoth.Name = "radBoth";
            this.radBoth.Size = new System.Drawing.Size(178, 22);
            this.radBoth.TabIndex = 7;
            this.radBoth.Text = "Good and Evil Themed";
            this.radBoth.UseVisualStyleBackColor = true;
            this.radBoth.CheckedChanged += new System.EventHandler(this.radBoth_CheckedChanged);
            this.radBoth.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.radBoth.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.radBoth.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // radEvil
            // 
            this.radEvil.AutoSize = true;
            this.radEvil.Font = new System.Drawing.Font("Book Antiqua", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radEvil.ForeColor = System.Drawing.Color.White;
            this.radEvil.Location = new System.Drawing.Point(6, 70);
            this.radEvil.Name = "radEvil";
            this.radEvil.Size = new System.Drawing.Size(110, 22);
            this.radEvil.TabIndex = 6;
            this.radEvil.TabStop = true;
            this.radEvil.Text = "Evil Themed";
            this.radEvil.UseVisualStyleBackColor = true;
            this.radEvil.CheckedChanged += new System.EventHandler(this.radEvil_CheckedChanged);
            this.radEvil.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.radEvil.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.radEvil.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // radGood
            // 
            this.radGood.AutoSize = true;
            this.radGood.Font = new System.Drawing.Font("Book Antiqua", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGood.ForeColor = System.Drawing.Color.White;
            this.radGood.Location = new System.Drawing.Point(6, 45);
            this.radGood.Name = "radGood";
            this.radGood.Size = new System.Drawing.Size(121, 22);
            this.radGood.TabIndex = 5;
            this.radGood.TabStop = true;
            this.radGood.Text = "Good Themed";
            this.radGood.UseVisualStyleBackColor = true;
            this.radGood.CheckedChanged += new System.EventHandler(this.radGood_CheckedChanged);
            this.radGood.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.radGood.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.radGood.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // radNo
            // 
            this.radNo.AutoSize = true;
            this.radNo.Font = new System.Drawing.Font("Book Antiqua", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radNo.ForeColor = System.Drawing.Color.White;
            this.radNo.Location = new System.Drawing.Point(6, 20);
            this.radNo.Name = "radNo";
            this.radNo.Size = new System.Drawing.Size(168, 22);
            this.radNo.TabIndex = 4;
            this.radNo.Text = "Old 2.01 Lobby Music";
            this.radNo.UseVisualStyleBackColor = true;
            this.radNo.CheckedChanged += new System.EventHandler(this.radNo_CheckedChanged);
            this.radNo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.radNo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.radNo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // lblStatus202
            // 
            this.lblStatus202.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus202.Font = new System.Drawing.Font("Book Antiqua", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus202.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.lblStatus202.Location = new System.Drawing.Point(40, 183);
            this.lblStatus202.Name = "lblStatus202";
            this.lblStatus202.Size = new System.Drawing.Size(223, 71);
            this.lblStatus202.TabIndex = 13;
            this.lblStatus202.Text = "...";
            this.lblStatus202.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblStatus202.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.lblStatus202.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.lblStatus202.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // lblVersion202
            // 
            this.lblVersion202.BackColor = System.Drawing.Color.Transparent;
            this.lblVersion202.Font = new System.Drawing.Font("Book Antiqua", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion202.ForeColor = System.Drawing.Color.White;
            this.lblVersion202.Location = new System.Drawing.Point(36, 254);
            this.lblVersion202.Name = "lblVersion202";
            this.lblVersion202.Size = new System.Drawing.Size(227, 89);
            this.lblVersion202.TabIndex = 12;
            this.lblVersion202.Text = "There is no version information.";
            this.lblVersion202.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblVersion202.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.lblVersion202.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.lblVersion202.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // bgw202
            // 
            this.bgw202.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw202_DoWork);
            this.bgw202.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgw202_RunWorkerCompleted);
            // 
            // butHelp
            // 
            this.butHelp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butHelp.BackColor = System.Drawing.Color.Transparent;
            this.butHelp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("butHelp.BackgroundImage")));
            this.butHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butHelp.FlatAppearance.BorderSize = 0;
            this.butHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butHelp.Font = new System.Drawing.Font("Bookman Old Style", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butHelp.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.butHelp.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butHelp.Location = new System.Drawing.Point(269, 209);
            this.butHelp.Name = "butHelp";
            this.butHelp.Size = new System.Drawing.Size(208, 40);
            this.butHelp.TabIndex = 3;
            this.butHelp.Text = "Help";
            this.butHelp.UseVisualStyleBackColor = false;
            this.butHelp.Click += new System.EventHandler(this.butHelp_Click);
            this.butHelp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.butHelp.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.butHelp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // butChangelog
            // 
            this.butChangelog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butChangelog.BackColor = System.Drawing.Color.Transparent;
            this.butChangelog.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("butChangelog.BackgroundImage")));
            this.butChangelog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butChangelog.FlatAppearance.BorderSize = 0;
            this.butChangelog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butChangelog.Font = new System.Drawing.Font("Bookman Old Style", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butChangelog.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.butChangelog.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butChangelog.Location = new System.Drawing.Point(269, 154);
            this.butChangelog.Name = "butChangelog";
            this.butChangelog.Size = new System.Drawing.Size(208, 40);
            this.butChangelog.TabIndex = 2;
            this.butChangelog.Text = "Tech Support";
            this.butChangelog.UseVisualStyleBackColor = false;
            this.butChangelog.Click += new System.EventHandler(this.butChangelog_Click);
            this.butChangelog.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.butChangelog.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.butChangelog.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // Form1
            // 
            this.AcceptButton = this.butRun;
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::VisualLauncher.Properties.Resources.rotwkuibackpic;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CancelButton = this.butExit;
            this.ClientSize = new System.Drawing.Size(520, 591);
            this.Controls.Add(this.cmbResolution);
            this.Controls.Add(this.butChangelog);
            this.Controls.Add(this.butHelp);
            this.Controls.Add(this.lblStatus202);
            this.Controls.Add(this.lblVersion202);
            this.Controls.Add(this.grpMusic);
            this.Controls.Add(this.chkEnable202);
            this.Controls.Add(this.butExit);
            this.Controls.Add(this.chkVideos);
            this.Controls.Add(this.lblResolution);
            this.Controls.Add(this.chkVersion);
            this.Controls.Add(this.cmbVersion);
            this.Controls.Add(this.butRun);
            this.Controls.Add(this.lblText);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(520, 591);
            this.MinimumSize = new System.Drawing.Size(520, 591);
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Patch Control";
            this.TransparencyKey = System.Drawing.Color.Fuchsia;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.grpMusic.ResumeLayout(false);
            this.grpMusic.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblText;
        private System.Windows.Forms.Button butRun;
        private System.Windows.Forms.ComboBox cmbResolution;
        private System.Windows.Forms.Label lblResolution;
        private System.Windows.Forms.ComboBox cmbVersion;
        private System.Windows.Forms.CheckBox chkVersion;
        private System.Windows.Forms.CheckBox chkVideos;
        private System.Windows.Forms.Button butExit;
        private System.Windows.Forms.CheckBox chkEnable202;
        private System.Windows.Forms.GroupBox grpMusic;
        private System.Windows.Forms.RadioButton radBoth;
        private System.Windows.Forms.RadioButton radEvil;
        private System.Windows.Forms.RadioButton radGood;
        private System.Windows.Forms.RadioButton radNo;
        private System.Windows.Forms.Label lblStatus202;
        private System.Windows.Forms.Label lblVersion202;
        private System.ComponentModel.BackgroundWorker bgw202;
        private System.Windows.Forms.Button butHelp;
        private System.Windows.Forms.Button butChangelog;
        private System.Windows.Forms.RadioButton radSmoke;
    }
}

