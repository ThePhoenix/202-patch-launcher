﻿namespace U202VisualLauncher
{
    partial class ChangelogWindow
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangelogWindow));
            this.txtChangelog = new System.Windows.Forms.TextBox();
            this.butCloseChangelog = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtChangelog
            // 
            this.txtChangelog.BackColor = System.Drawing.Color.Black;
            this.txtChangelog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChangelog.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtChangelog.Location = new System.Drawing.Point(73, 85);
            this.txtChangelog.Multiline = true;
            this.txtChangelog.Name = "txtChangelog";
            this.txtChangelog.ReadOnly = true;
            this.txtChangelog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtChangelog.ShortcutsEnabled = false;
            this.txtChangelog.Size = new System.Drawing.Size(373, 398);
            this.txtChangelog.TabIndex = 0;
            // 
            // butCloseChangelog
            // 
            this.butCloseChangelog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.butCloseChangelog.BackColor = System.Drawing.SystemColors.Control;
            this.butCloseChangelog.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("butCloseChangelog.BackgroundImage")));
            this.butCloseChangelog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.butCloseChangelog.FlatAppearance.BorderSize = 0;
            this.butCloseChangelog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.butCloseChangelog.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butCloseChangelog.ForeColor = System.Drawing.Color.LightSteelBlue;
            this.butCloseChangelog.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butCloseChangelog.Location = new System.Drawing.Point(173, 516);
            this.butCloseChangelog.Name = "butCloseChangelog";
            this.butCloseChangelog.Size = new System.Drawing.Size(178, 26);
            this.butCloseChangelog.TabIndex = 16;
            this.butCloseChangelog.Text = "Close";
            this.butCloseChangelog.UseVisualStyleBackColor = false;
            this.butCloseChangelog.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.butCloseChangelog.Click += new System.EventHandler(this.butCloseChangelog_Click);
            this.butCloseChangelog.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.butCloseChangelog.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            // 
            // ChangelogWindow
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackgroundImage = global::VisualLauncher.Properties.Resources.rotwkuibackpic;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(520, 591);
            this.Controls.Add(this.butCloseChangelog);
            this.Controls.Add(this.txtChangelog);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(520, 591);
            this.MinimumSize = new System.Drawing.Size(520, 591);
            this.Name = "ChangelogWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Changelog";
            this.TransparencyKey = System.Drawing.Color.Fuchsia;
            this.Load += new System.EventHandler(this.ChangelogWindow_Load);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox txtChangelog;
        internal System.Windows.Forms.Button butCloseChangelog;

    }
}